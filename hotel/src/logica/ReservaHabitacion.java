/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logica;

import java.util.Date;

/**
 *
 * @author
 */
public class ReservaHabitacion {

    /**
     * inicializacion de variables V_cliente variable de tipo cliente
     * V_habitacion variable para la habitacion V_habitacion fecha inicial
     * V_fecha_final fecha final V_numeroReserva numero de la reserva
     */
    private Cliente V_cliente;
    private Habitacion V_habitacion;

    private Date V_fecha_inicial;
    private Date V_fecha_final;

    private String V_numeroReserva;

    // <editor-fold defaultstate="collapsed" desc="get and set">
    /**
     * retorna el valor del la clase cliente
     *
     * @return retorna cliente
     */
    public Cliente getV_cliente() {
        return V_cliente;
    }

    /**
     * asigna un valor al cliente
     *
     * @param V_cliente asigna los valores al cliente
     */
    public void setV_cliente(Cliente V_cliente) {
        this.V_cliente = V_cliente;
    }

    /**
     * metodo que retorna los valores de la clase habitacion
     *
     * @return retorna habitacion
     */
    public Habitacion getV_habitacion() {
        return V_habitacion;
    }

    /**
     * metodo que asigna valor a habitacion
     *
     * @param V_habitacion asigna valor a habitacion
     */
    public void setV_habitacion(Habitacion V_habitacion) {
        this.V_habitacion = V_habitacion;
    }

    /**
     * metodo que retorna l valor de la fecha inicial de la reservacion
     *
     * @return retorna el valor de la fecha inicial
     */
    public Date getV_fecha_inicial() {
        return V_fecha_inicial;
    }

    /**
     * metodo que asigna un valor a la fecha inicial de reservacion
     *
     * @param V_fecha_inicial asifna valor a la fecha inicial
     */
    public void setV_fecha_inicial(Date V_fecha_inicial) {
        this.V_fecha_inicial = V_fecha_inicial;
    }

    /**
     * metodo que asigna un valor a la fecha final de la reservacion
     *
     * @return retorna la fecha final de reservacion
     */
    public Date getV_fecha_final() {
        return V_fecha_final;
    }

    /**
     * metodo que signa valor a la fecha final de la reservacion
     *
     * @param V_fecha_final asigna valor a la fecha final de reservacion
     */
    public void setV_fecha_final(Date V_fecha_final) {
        this.V_fecha_final = V_fecha_final;
    }

    /**
     * metodo que retorna el numero de la reserva
     *
     * @return retorna numero de reserva
     */
    public String getV_numeroReserva() {
        return V_numeroReserva;
    }

    /**
     * metodo que asigna valor al numero de la reserva
     *
     * @param V_numeroReserva asigna valor a numero de reserva
     */
    public void setV_numeroReserva(String V_numeroReserva) {
        this.V_numeroReserva = V_numeroReserva;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="constructor">
    /**
     * metodo constructor para llenar todos los atributos/variables
     *
     * @param p_cliente asigna valor a cliente
     * @param p_habitacion asigna valor a habitacion
     * @param p_fecha_inicial asigna valor a fecha inicial
     * @param p_fecha_finalasigna valor a fecha final
     */
    public ReservaHabitacion(Cliente p_cliente, Habitacion p_habitacion, Date p_fecha_inicial, Date p_fecha_final) {
        this.V_cliente = p_cliente;
        this.V_habitacion = p_habitacion;
        this.V_fecha_inicial = p_fecha_inicial;
        this.V_fecha_final = p_fecha_final;
    }
    // </editor-fold>
}
