/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logica;

/**
 *
 * @author Admin
 */
public class Impresion {

    /**
     * inicializacion de variables V_numeroFactura = numero de la factura
     * V_fecha = fecha V_idCliente = id del cliente V_concepto = concepto de
     * cobro V_subtotal = sub total a pagar V_iva = IVA V_total = total a pagar
     * V_cliente = nombre y apellido del cliente
     */
    private String V_numeroFactura;
    private String V_fecha;
    private int V_idCliente;
    private String V_concepto = "Hab. Sencilla";
    private int V_subtotal;
    private String V_iva = "19%";
    private int V_total;
    private String V_cliente = "";

    // <editor-fold defaultstate="collapsed" desc="get and set">
    /**
     * metodo que retorna el valor de numero de la factura
     *
     * @return retorna V_numeroFactura
     */
    public String getV_numeroFactura() {
        return V_numeroFactura;
    }

    /**
     * metodo que asigna un valor al numero de la factura
     *
     * @param V_numeroFactura asigna valor a V_numeroFactura
     */
    public void setV_numeroFactura(String V_numeroFactura) {
        this.V_numeroFactura = V_numeroFactura;
    }

    /**
     * metodo que retorna el valor de la fecha de la factura
     *
     * @return retorna V_fecha
     */
    public String getV_fecha() {
        return V_fecha;
    }

    /**
     * metodo que asigna un valor a la fecha de la factura
     *
     * @param V_fecha asigna valor a V_fecha
     */
    public void setV_fecha(String V_fecha) {
        this.V_fecha = V_fecha;
    }

    /**
     * metodo que retorna el valor el id del cliente
     *
     * @return retorna V_idCliente
     */
    public int getV_idCliente() {
        return V_idCliente;
    }

    /**
     * metodo que asigna valor al id del cliente
     *
     * @param V_idCliente asigna valor a V_idCliente
     */
    public void setV_idCliente(int V_idCliente) {
        this.V_idCliente = V_idCliente;
    }

    /**
     * metodo que retorna el valor del concepto
     *
     * @return retorna getV_concepto
     */
    public String getV_concepto() {
        return V_concepto;
    }

    /**
     * metodo que asigna un valor al concepto a cobrar
     *
     * @param V_concepto asigna valor a V_concepto
     */
    public void setV_concepto(String V_concepto) {
        this.V_concepto = V_concepto;
    }

    /**
     * metodo que retorna el valor del subtotal de la factura
     *
     * @return retorna V_subtotal
     */
    public int getV_subtotal() {
        return V_subtotal;
    }

    /**
     * metodo que asigna un valor al subtotal
     *
     * @param V_subtotal asigna valor a V_subtotal
     */
    public void setV_subtotal(int V_subtotal) {
        this.V_subtotal = V_subtotal;
    }

    /**
     * metodo que retorna el valor del iva
     *
     * @return retorna IVA
     */
    public String getV_iva() {
        return V_iva;
    }

    /**
     * metodo que asigna un valor al IVA
     *
     * @param V_iva asigna valor a V_iva
     */
    public void setV_iva(String V_iva) {
        this.V_iva = V_iva;
    }

    /**
     * metodo que retorna el total a pagar
     *
     * @return retorna V_total
     */
    public int getV_total() {
        return V_total;
    }

    /**
     * metodo que asigna un valor al total a pagar
     *
     * @param V_total asigna un valor a V_total
     */
    public void setV_total(int V_total) {
        this.V_total = V_total;
    }

    /**
     * metoto que retorna el nombre del cliente
     *
     * @return retorna V_cliente
     */
    public String getV_cliente() {
        return V_cliente;
    }

    /**
     * metodo que asigna un valor al nombre del cliente
     *
     * @param V_cliente asigna valor a V_liente
     */
    public void setV_cliente(String V_cliente) {
        this.V_cliente = V_cliente;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="metodos personalizados">
    /**
     * metodo que asigna y retorna los valores de la impresion de la factura
     *
     * @param CadenaTexto texto que llena los atributos
     * @return retorna las variables llenas
     */
    public String impresionconsola(String CadenaTexto) {
        return (CadenaTexto);
    }

    /**
     * metodo que retorna la factura
     *
     * @return retorna la factura llena
     */
    public String ImpresionFactura() {

        return impresionconsola("Numero de factura: " + getV_numeroFactura() + "\n")
                + impresionconsola("Fecha: " + getV_fecha() + "\n")
                + impresionconsola("Cliente: " + getV_cliente() + "\n")
                + impresionconsola("ID/NIT cliente: " + getV_idCliente() + "\n")
                + impresionconsola("Concepto: " + getV_concepto() + "\n")
                + impresionconsola("subtotal: " + getV_subtotal() + "\n")
                + impresionconsola("IVA: " + getV_iva() + "\n")
                + impresionconsola("total: " + getV_total() + "\n");
    }

    /**
     * metodo para asignar los valores para generar la factura
     *
     * @param p_numeroFact asigna el numero de factura
     * @param p_fecha asigna la fecha de la generacion de la factura
     * @param p_cliente asigna el nombre del cliente
     * @param p_idCliente asigna la identificacion del cliente
     * @param p_concepto asigna el concepto de cobro
     * @param p_subtotal asigna el subtotal a pagar
     * @param p_total asigna el total a pagar
     */
    public Impresion(String p_numeroFact, String p_fecha, String p_cliente, int p_idCliente, int p_subTotal, int p_total) {
        this.V_numeroFactura = p_numeroFact;
        this.V_fecha = p_fecha;
        this.V_cliente = p_cliente;
        this.V_idCliente = p_idCliente;
        this.V_subtotal = p_subTotal;
        this.V_total = p_total;
    }
    // </editor-fold>

}
