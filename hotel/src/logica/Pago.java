/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logica;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Admin
 */
public class Pago {

    /**
     * inicializacion de variables V_subtotal almacenara el subtotal de la
     * factura V_total almacenara el total a pagar de la factura
     */
    private int V_subtotal;
    private int V_total;
    public static int V_valorHospedaje = 40000;

    // <editor-fold defaultstate="collapsed" desc="get y set">
    /**
     * metodo que retorna el valor del subtotal
     *
     * @return retorna V_subtotal
     */
    public int getV_subtotal() {
        return V_subtotal;
    }

    /**
     * metodo que asigna valor al subtotal
     *
     * @param V_subtotal asigna valor al subtotal
     */
    public void setV_subtotal(int V_subtotal) {
        this.V_subtotal = V_subtotal;
    }

    /**
     * metodo que retorna el total a pagar
     *
     * @return retorna V_total
     */
    public int getV_total() {
        return V_total;
    }

    /**
     * metodo que asigna un valor al total a pagar
     *
     * @param V_total asigna valor total a pagar
     */
    public void setV_total(int V_total) {
        this.V_total = V_total;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="metodos personalizados">
    /**
     * metodo que calcula el subtotal
     *
     * @param p_fechainicial fecha inicial para comparar los dias
     * @param p_fechafinal fecha final para comparar dias
     * @return retorna el valor del subtotal
     */
    public int calcularSubtotal(Date p_fechainicial, Date p_fechafinal) {
        int V_diasHospedaje = 0;
        String V_formato = "dd";
        SimpleDateFormat Obj_formatoDias = new SimpleDateFormat(V_formato);

        if (Integer.parseInt(Obj_formatoDias.format(p_fechainicial)) < Integer.parseInt(Obj_formatoDias.format(p_fechafinal))) {

            for (int i = 0; i <= Integer.parseInt(Obj_formatoDias.format(p_fechafinal)); i++) {

                V_diasHospedaje = i - Integer.parseInt(Obj_formatoDias.format(p_fechainicial));
            }
            return (V_diasHospedaje * V_valorHospedaje);
        }
        return 0;
    }

    /**
     * metodo que retorna el total a pagar
     *
     * @param p_fechainicial fecha inicial para obtener el subtotal
     * @param p_fechafinal fecha final para obtener el subtotal
     * @return retorna el valor total a pagar
     */
    public int calcularTotal(Date p_fechainicial, Date p_fechafinal) {

        int V_subtotal = calcularSubtotal(p_fechainicial, p_fechafinal);

        int V_total = (int) ((V_subtotal * 0.19) + V_subtotal);

        return V_total;

    }

    // </editor-fold>
}
