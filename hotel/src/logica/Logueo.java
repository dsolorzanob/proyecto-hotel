/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logica;

/**
 *
 * @author
 */
public class Logueo {

    /**
     * inicializacion de variables: _username nombre de usuario _password
     * contraseña de un usuario
     */
    private String V_username;
    private String V_password;

    // <editor-fold defaultstate="collapsed" desc="get and set">
    /**
     * metodo que retorna el nombre de usuario
     *
     * @return retorna _username
     */
    public String getUsername() {
        return V_username;
    }

    /**
     * metodo que asigna un valor al nombre del usuario
     *
     * @param username asigna valor a _username
     */
    public void setUsername(String username) {
        this.V_username = username;
    }

    /**
     * metodo que retorna el valor de la contraseña de un usuario
     *
     * @return retorna _password
     */
    public String getPassword() {
        return V_password;
    }

    /**
     * metodo que asigna un valor a la contraseña de un usuario
     *
     * @param password asigna un valor a _password
     */
    public void setPassword(String password) {
        this.V_password = password;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="metodos personalizados">
    /**
     * metodo que verifica las credenciales para iniciar sesion en el login
     *
     * @param username parametro del nombre de usuario
     * @param password parametro de la contraseña
     * @return retorna si las credenciales son correctas o no lo son
     */
    public boolean logueo(String p_username, String p_password) {

        return (this.V_username.equals(p_username) && this.V_password.equals(p_password));
    }

    /**
     * metodo para crear un usuario
     *
     * @param username parametro de nombre de usuario
     * @param password parametro de contraseña de usuario
     */
    public void crearUsuario(String p_username, String p_password) {
        this.V_username = p_username;
        this.V_password = p_password;
    }
    // </editor-fold>
}
