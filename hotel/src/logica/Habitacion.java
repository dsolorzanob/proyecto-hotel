package logica;

/**
 *
 * @author
 */
public class Habitacion {

    /**
     * inicializacion de variables V_disponibilidad disponibilidad de una
     * habitacion numero_habitacion representa el numero de la habitacion
     */
    //habitacion individual o sencilla
    private boolean V_disponibilidadhabitacion;
    private int v_numerohabitacion;

    // <editor-fold defaultstate="collapsed" desc="get and set">
    public boolean isDisponibilidad_habitacion() {
        return V_disponibilidadhabitacion;
    }

    public void setDisponibilidad_habitacion(boolean disponibilidad_habitacion) {
        this.V_disponibilidadhabitacion = disponibilidad_habitacion;
    }

    public int getNumero_habitacion() {
        return v_numerohabitacion;
    }

    public void setNumero_habitacion(int numero_habitacion) {
        this.v_numerohabitacion = numero_habitacion;
    }
    // </editor-fold>

}
