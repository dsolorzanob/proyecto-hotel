package logica;

/**
 *
 * @author
 */
public class Cliente {

    /**
     * inicializacion de variables _id numero de identificacion de un cliente
     * _nombre nombre del cliente _apellido apellido del cliente _habitacion es
     * para identificar si se va aocupar una habitacion
     */
    private int V_id;
    private String V_nombre;
    private String V_apellido;

    // <editor-fold defaultstate="collapsed" desc="metodo constructor">
    /**
     * metodo constructor de la clase, instancia los valores iniciales para los
     * atributos
     */
    public Cliente() {
        V_id = 0;
        V_nombre = null;
        V_apellido = null;

    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="get and set">
    /**
     * metodo que retorna el numero de identificacion de un cliente
     *
     * @return retornar el valor de Vid
     */
    public int getId() {
        return V_id;
    }

    /**
     * metodo que guarda el numero de la identificacion de un cliente
     *
     * @param _id asignar valor a Vid
     */

    public void setId(int _id) {
        this.V_id = _id;
    }

    /**
     * metodo que muestra el nombre del cliente
     *
     * @return retornar Vnombre
     */
    public String getNombre() {
        return V_nombre;
    }

    /**
     * metodo que guarda el nombre del cliente
     *
     * @param _nombre asigna el valor a Vnombre
     */
    public void setNombre(String _nombre) {
        this.V_nombre = _nombre;
    }

    /**
     * metodo que retorna el apellido del cliente
     *
     * @return retornar Vapellido
     */
    public String getApellido() {
        return V_apellido;
    }

    /**
     * metodo que asigna un valor al _apellido de un cliente
     *
     * @param _apellido asigna el valor a Vapellido
     */
    public void setApellido(String _apellido) {
        this.V_apellido = _apellido;
    }

    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="metodos personalizados">
    /**
     * metodo para registrar cliente en una habitacion
     *
     * @param id parametro del id
     * @param nombre parametro nombre
     * @param apellido parametro apellido
     */
    public void registrar(int p_id, String p_nombre, String p_apellido) {
        this.V_id = p_id;
        this.V_nombre = p_nombre;
        this.V_apellido = p_apellido;

    }
    // </editor-fold>

}
