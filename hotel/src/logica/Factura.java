/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logica;

import java.util.Date;

/**
 *
 * @author Admin
 */
public class Factura {

    // <editor-fold defaultstate="collapsed" desc="metodo de verificacion de la factura">
    /**
     * metodo que verifica los elementos del arreglo de reservaciones para
     * generar la factura
     *
     * @param p_idCliente parametro con los datos del cliente
     * @param p_fechaInicial parametro con la fecha inicial a comparar
     * @param p_fechaFinal parametro con la fecha final a comparar
     * @return retorna la reservacion que cumpla la sentencia
     */
    public ReservaHabitacion generarFactura(int p_idCliente, Date p_fechaInicial, Date p_fechaFinal) {

        for (ReservaHabitacion i : Registro.V_registro) {
            if (i.getV_cliente().getId() == p_idCliente && (i.getV_fecha_inicial().compareTo(p_fechaInicial) > 0
                    && i.getV_fecha_final().compareTo(p_fechaFinal) < 0)) {
                return i;
            }
        }
        return null;
    }
    // </editor-fold>

}
